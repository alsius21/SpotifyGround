/** @type {import('next').NextConfig} */
const nextConfig = {
  productionBrowserSourceMaps: true,
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'mosaic.scdn.co',
      },
      {
        protocol: 'https',
        hostname: 'i.scdn.co',
      },
    ],
  },
  env: {
    NEXT_PUBLIC_SPOTIFY_SCOPE: process.env.NEXT_PUBLIC_SPOTIFY_SCOPE,
  },
};

export default nextConfig;
