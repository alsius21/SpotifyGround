import type { Config } from 'jest';
import nextJest from 'next/jest.js';

const createJestConfig = nextJest({
  dir: './',
});

const config: Config = {
  // The root of your source code, typically /src
  // `<rootDir>` is a token Jest substitutes
  roots: ['<rootDir>/src/'],
  preset: 'ts-jest',

  // Jest transformations -- this adds support for TypeScript
  // using ts-jest
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
    '^.+\\.(ts|tsx)?$': 'ts-jest',
    '^.+\\.(css|less)$': './src/mocks/styleMock.ts',
    // '^.+\\.svg$': 'jest-svg-transformer',
  },
  testEnvironment: 'jsdom',

  // Runs special logic, such as cleaning up components
  // when using React Testing Library and adds special
  // extended assertions to Jest
  setupFilesAfterEnv: ['<rootDir>/src/test/setupTests.ts'],

  // Test spec file resolution pattern
  // Matches parent folder `__tests__` and filename
  // should contain `test` or `spec`.
  testRegex: '(/(__tests__)/.*|(\\.|/)(test|spec))\\.(ts|tsx)?$',

  // Module file extensions for importing
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  moduleNameMapper: {
    '@/(.*)$': '<rootDir>/src/$1',
  },
  coverageProvider: 'v8',
  coveragePathIgnorePatterns: [
    'node_modules',
    'src/mocks',
    'src/test',
    'src/translations',
    '.*\\.test\\.(js|ts|tsx|jsx)',
    '.*\\.spec\\.(js|ts|tsx|jsx)',
  ],
};

export default createJestConfig(config);
