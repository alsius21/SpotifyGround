'use server';
import { getCurrentUsersProfileSchema } from '@/_models/spotify/getCurrentUsersProfile.schema';
import spotifyHelper, { endpoint, Endpoint } from '@/_services/spotify.helper';
import { getAuthHeaders } from './auth.helper';

export const getPlaylist = async (
  id: string,
): Promise<SpotifyApi.SinglePlaylistResponse> => {
  const url = new URL(endpoint(Endpoint.Playlist).replace(':id', id));
  return fetch(url.toString(), {
    method: 'GET',
    headers: await getAuthHeaders(),
    cache: 'force-cache',
  }).then((response) => response.json());
};

export const getCurrentUserProfile = async () => {
  console.debug('getCurrentUserProfile', endpoint(Endpoint.Me));
  const response = await fetch(endpoint(Endpoint.Me), {
    method: 'GET',
    headers: await getAuthHeaders(),
    cache: 'force-cache',
  });
  const rawProfile = await response.json();
  return getCurrentUsersProfileSchema.parse(rawProfile);
};

export const getCurrentUserPlaylists = async (
  offset: number,
  limit: number = spotifyHelper.PAGE_SIZE,
): Promise<SpotifyApi.ListOfCurrentUsersPlaylistsResponse> => {
  const url = new URL(endpoint(Endpoint.CurrentUserPlaylists));
  url.searchParams.set('limit', limit.toString());
  url.searchParams.set('offset', offset.toString());
  const response = await fetch(url.toString(), {
    method: 'GET',
    headers: await getAuthHeaders(),
    cache: 'force-cache',
  });
  return await response.json();
};

type TrackOrTracks =
  | {
      uris: string[];
    }
  | {
      context_uri: string;
    };
type TrackPosition = {
  position_ms?: number;
  offset?: { position: number };
};

type StartOrResumePlaybackPayload = TrackOrTracks & TrackPosition;
type StartOrResumePlaybackRequest = {
  device_id?: string;
  payload: StartOrResumePlaybackPayload;
};

export const startResumePlayback = async (
  request: StartOrResumePlaybackRequest,
) => {
  const url = new URL(endpoint(Endpoint.StartOrResume));
  if (request.device_id) {
    url.searchParams.set('device_id', request.device_id);
  }

  const init = {
    method: 'PUT',
    body: JSON.stringify(request.payload),
    headers: await getAuthHeaders(),
  };

  return fetch(url.toString(), init);
};
