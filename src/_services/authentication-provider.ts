import { authOptions } from '@/app/api/auth/[...nextauth]/authOptions';
import { getServerSession } from 'next-auth';

export const getAccessToken = async () => {
  const session = await getServerSession(authOptions);
  if (!session) {
    throw new Error('No session found');
  }
  return session.accessToken;
};
