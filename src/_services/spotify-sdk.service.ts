import {
  PlayerStateChangedResponse,
  WebPlaybackStateObject,
  WebPlaybackErrorEnum,
  WebPlaybackReadinessEnum,
  EventDevice,
  EventMessage,
  WebplaybackEventsRecordType,
  WebPlaybackFeedbackEnum,
} from '@/_models/spotify/webplaybackSDK';
import toast from 'react-hot-toast';
import { diff } from 'json-diff-ts';
import i18next from './i18n/client';

type EventCallback<T> = (event: T) => void;

const { Account, Playback, Authentication, Initialization } =
  WebPlaybackErrorEnum;

const { PlayerStateChanged, AutoPlayFailed } = WebPlaybackFeedbackEnum;

type UnknownFunction = (e: unknown) => unknown;

/**
 * @see https://developer.spotify.com/documentation/web-playback-sdk/reference/#api-spotify-player
 *
 */
type SDKEventHandler = <T extends keyof WebplaybackEventsRecordType>(
  event: T,
  cb: WebplaybackEventsRecordType[T],
) => void;

declare module Spotify {
  class Player {
    constructor(config: {
      name: string;
      volume: number;
      getOAuthToken: (cb: (token: string) => void) => void;
    });
    addListener: SDKEventHandler;
    connect(): Promise<boolean>;
    disconnect(): void;
    getCurrentState(): Promise<WebPlaybackStateObject | null>;
    getVolume(): Promise<number>; // (as a Float between 0 and 1).
    nextTrack(): Promise<void>;
    on(e: unknown, t: unknown): unknown;
    pause(): Promise<void>;
    previousTrack(): Promise<void>;
    removeListener(e: unknown, t: unknown): boolean;
    resume(): Promise<void>;
    seek(position_ms: number): Promise<void>; // the position in milliseconds to seek to.
    setName(name: string): Promise<void>;
    setVolume(volume: number): Promise<void>;
    togglePlay(): Promise<void>;
    activateElement(): Promise<void>; //Some browsers prevent autoplay of media by ensuring that all playback is triggered by synchronous event-paths originating from user interaction such as a click. In the autoplay disabled browser, to be able to keep the playing state during transfer from other applications to yours, this function needs to be called in advance. Otherwise it will be in pause state once it’s transferred.
    _getListeners: UnknownFunction;
    _handleMessages(e: unknown, t: unknown, s: unknown): unknown;
    _onConnected: UnknownFunction;
    _onCurrentState: UnknownFunction;
    _onEvent: UnknownFunction;
    _onGetToken(e: unknown, t: unknown): unknown;
    _onVolume: UnknownFunction;
    _sendMessage: UnknownFunction;
    _sendMessageWhenLoaded: UnknownFunction;
    isLoaded: Promise<unknown>;
    _connectionRequests: Record<string, unknown>;
    _eventListeners: Record<string, ((args: unknown) => unknown)[]>;
    _getCurrentStateRequests: Record<string, unknown>;
    _getVolumeRequests: Record<string, unknown>;
    _messageHandlers: {
      ASK_ID_CARDED: UnknownFunction;
      CURRENT_STATE: UnknownFunction;
      EVENT: UnknownFunction;
      GET_TOKEN: UnknownFunction;
      VOLUME: UnknownFunction;
    };
    _options: PlayerOptions & { id?: string };
  }
  interface PlayerOptions {
    name: string;
    getOAuthToken: (cb: (token: string) => void) => void;
    volume?: number;
    enableMediaSession?: boolean;
  }
}

declare global {
  interface Window {
    onSpotifyWebPlaybackSDKReady: () => void;
    Spotify: typeof Spotify;
  }
}

type EventListenerCallback =
  | EventCallback<EventMessage>
  | EventCallback<EventDevice>
  | EventCallback<PlayerStateChangedResponse>;
class SpotifySDKService {
  volume: number = 0.8;
  instance: Spotify.Player | undefined = undefined;
  device_id: string | undefined = undefined;
  lastState: PlayerStateChangedResponse | undefined = undefined;
  listeners: {
    event: string;
    cb: EventListenerCallback;
  }[] = [];

  setup = (
    token: string,
    setLastState: (lastState: PlayerStateChangedResponse) => void,
  ): void => {
    delete this.instance;
    console.info("Setting up Spotify's Web Playback SDK");

    window.onSpotifyWebPlaybackSDKReady =
      window.onSpotifyWebPlaybackSDKReady || null;

    window.onSpotifyWebPlaybackSDKReady = () => {
      if (!token) {
        throw Error('Invalid state: No token found');
      }
      this.instance = new Spotify.Player({
        name: 'Web Playback SDK Quick Start Player',
        volume: this.volume,
        getOAuthToken: (cb: (token: string) => void) => {
          cb(token);
        },
      });
      this.setupEventListeners(setLastState);
      this.connectToSDK();
      this.initializeState(setLastState);
      this.configurePlayer();
      this.handleAutoPlayFailures();
    };
  };

  cleanup = (): void => {
    this.removeEventListeners();
    this.disconnectFromSDK();
    this.instance = undefined;
  };

  seek = (position: number): void => {
    this.instance
      ?.seek(position)
      .then(() => console.debug('seek ok'))
      .catch(console.error);
  };

  enable = (): void => {
    this.instance
      ?.activateElement()
      .then(() => console.debug('enable ok'))
      .catch(console.error);
  };

  private setupEventListeners = (
    setLastState: (lastState: WebPlaybackStateObject) => void,
  ): void => {
    [Account, Playback, Authentication, Initialization].forEach((key) => {
      const errorCb: EventCallback<EventMessage> = ({ message }) => {
        const translationKey = 'error.' + key;
        this.notify(translationKey, 'error');
        console.error(translationKey, message);
      };
      this.instance?.addListener(key, errorCb);
      this.storeListener(key, errorCb);
    });

    const readyCb: EventCallback<{ device_id: string }> = ({
      device_id,
    }: EventDevice) => {
      console.log('readiness.success', { device_id });
      this.device_id = device_id;
      this.notify('readiness.notification.success', 'success');
    };
    this.instance?.addListener(WebPlaybackReadinessEnum.Ready, readyCb);
    this.storeListener(WebPlaybackReadinessEnum.Ready, readyCb);

    const notReadyCb: EventCallback<EventDevice> = ({ device_id }) => {
      console.log('readiness.failure', device_id);
      this.notify('readiness.notification.failure', 'error');
    };
    this.instance?.addListener(WebPlaybackReadinessEnum.NotReady, notReadyCb);
    this.storeListener(WebPlaybackReadinessEnum.NotReady, notReadyCb);

    const playerStateChangedCb: EventCallback<PlayerStateChangedResponse> = (
      state,
    ) => {
      console.debug('difference', diff(state, this.lastState));
      this.lastState = state;
      setLastState(state);
    };

    this.instance?.addListener(PlayerStateChanged, playerStateChangedCb);
    this.storeListener(PlayerStateChanged, playerStateChangedCb);
  };

  private connectToSDK = (): void => {
    this.instance
      ?.connect()
      .then(() => console.info('connectivity.success'))
      .catch(() => console.warn('connectivity.failure'));
  };

  private initializeState = (
    setLastState: (state: WebPlaybackStateObject) => void,
  ): void => {
    this.instance
      ?.getCurrentState()
      .then((state: WebPlaybackStateObject | null) => {
        if (!state) {
          console.warn('playback.external');
        } else {
          setLastState(state);
        }
      });
  };

  private configurePlayer = (): void => {
    this.instance
      ?.setName('CD Sorpresa')
      .then(
        () => this.notify('configuration.success', 'success'),
        () => this.notify('configuration.failure', 'error'),
      )
      .catch(() => this.notify('configuration.failure', 'error'));
  };

  private handleAutoPlayFailures = (): void => {
    const autofailedCb = () => {
      console.warn('autoplay.failure');
    };
    this.instance?.addListener(AutoPlayFailed, autofailedCb);
    this.storeListener(AutoPlayFailed, autofailedCb);
  };

  private removeEventListeners = (): void => {
    this.listeners.forEach(
      ({ event, cb }) => this.instance?.removeListener(event, cb),
    );
  };

  private disconnectFromSDK = (): void => {
    this.instance?.disconnect();
  };

  private storeListener = <T extends keyof WebplaybackEventsRecordType>(
    event: T,
    cb: EventListenerCallback,
  ): void => {
    if (!this.listeners.find((l) => l.event === event)) {
      this.listeners.push({ event, cb });
    }
  };

  private notify = (message: string, type: 'error' | 'success'): void => {
    toast[type](i18next.t(`webplayback:${message}`));
  };
}

export const service = new SpotifySDKService();
