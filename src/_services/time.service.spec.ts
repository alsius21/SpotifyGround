import { msToHHMMSS } from './time.service';
describe('miliseconds to HH:MM:SS', () => {
  it('should return 00:00', () => {
    expect(msToHHMMSS(0)).toBe('00:00');
  });

  it('should return 00:01', () => {
    expect(msToHHMMSS(1000)).toBe('00:01');
  });
  it('should return 01:00:00', () => {
    expect(msToHHMMSS(3600000)).toBe('01:00:00');
  });
});
