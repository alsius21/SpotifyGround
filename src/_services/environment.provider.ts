import IEnvironmentProvider from './ienvironment.provider';
import { z } from 'zod';

const CredentialsSchema = z.object({
  clientId: z.string(),
  clientSecret: z.string(),
});

const getClientInfo = () => {
  return CredentialsSchema.parse({
    clientId: process.env.SPOTIFY_CLIENT_ID,
    clientSecret: process.env.SPOTIFY_CLIENT_SECRET,
  });
};

const getScope = () => {
  return process.env.NEXT_PUBLIC_SPOTIFY_SCOPE ?? '';
};
const getIsProd = () => {
  return process.env.NODE_ENV === 'production';
};
const getScopes = () => {
  return getScope().split(' ');
};

const environmentProvider: IEnvironmentProvider = {
  getScope,
  getIsProd,
  getScopes,
  getClientInfo,
};

export default environmentProvider;
