import connect from './connect.json';
import header from './header.json';
import webplayback from './webplayback.json';
import welcome from './welcome.json';
import navigation from './navigation.json';
export default { connect, header, webplayback, welcome, navigation } as const;
