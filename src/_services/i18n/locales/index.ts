import ca from './ca';
import en from './en';
import es from './es';

export default {
  ca,
  en,
  es,
} as const;
