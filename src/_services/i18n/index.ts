import { createInstance } from 'i18next';

import { initReactI18next } from 'react-i18next/initReactI18next';
import { getOptions, Language, Namespace } from './settings';

export const initI18next = async (
  lng: Language,
  ns: Namespace,
  i18n?: ReturnType<typeof createInstance>,
) => {
  const i18nInstance = i18n ?? createInstance();
  await i18nInstance.use(initReactI18next).init(getOptions(lng, ns));
  return i18nInstance;
};

export async function useTranslation(lng: Language, ns: Namespace) {
  const i18nextInstance = await initI18next(lng, ns);
  return {
    t: i18nextInstance.getFixedT<Namespace>(
      lng,
      Array.isArray(ns) ? ns[0] : ns,
    ),
    i18n: i18nextInstance,
  };
}
