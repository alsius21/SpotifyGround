import translations from './locales';
export enum Language {
  Catalan = 'ca',
  Spanish = 'es',
  English = 'en',
}
export enum Namespace {
  Common = 'common',
  WebPlayback = 'webplayback',
  Header = 'header',
  Welcome = 'welcome',
  Connect = 'connect',
  Navigation = 'navigation',
}

export const fallbackLng = Language.Catalan;
export const languages = [fallbackLng, Language.Spanish, Language.English];
export const defaultNS = 'common';
export const cookieName = 'i18next';
export const ns = [
  Namespace.Common,
  Namespace.WebPlayback,
  Namespace.Header,
  Namespace.Welcome,
  Namespace.Connect,
  Namespace.Navigation,
];

export function getOptions(lng = fallbackLng, ns = defaultNS) {
  return {
    // debug: true,
    supportedLngs: languages,
    fallbackLng,
    lng,
    fallbackNS: defaultNS,
    defaultNS,
    ns,
    resources: translations,
  };
}
