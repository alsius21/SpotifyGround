export const msToHHMMSS = (ms: number): string => {
  // Convert milliseconds to seconds
  const totalSeconds = Math.floor(ms / 1000);

  // Extract hours, minutes, and seconds
  const hours = Math.floor(totalSeconds / 3600);
  const minutes = Math.floor((totalSeconds % 3600) / 60);
  const seconds = totalSeconds % 60;

  // Format the time string
  let timeString = '';

  if (hours > 0) {
    timeString += hours.toString().padStart(2, '0') + ':';
  }

  timeString += minutes.toString().padStart(2, '0') + ':';
  timeString += seconds.toString().padStart(2, '0');

  return timeString;
};
