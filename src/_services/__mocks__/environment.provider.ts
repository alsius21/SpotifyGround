import IEnvironmentProvider from '@/_services/ienvironment.provider';

class EnvironmentProvider implements IEnvironmentProvider {
  getClientId = jest.fn();
  getScope = jest.fn();
  getIsProd = jest.fn();
}

export default EnvironmentProvider;
