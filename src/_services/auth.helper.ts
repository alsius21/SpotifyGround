import { getAccessToken } from './authentication-provider';

export const getAuthHeaders = async () => {
  const accessToken = await getAccessToken();
  return {
    Authorization: 'Bearer ' + accessToken,
    'Content-Type': 'application/json',
  };
};
