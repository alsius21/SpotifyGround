const baseAuthorizationUrl = 'https://accounts.spotify.com/authorize';
const baseApiUrl = 'https://api.spotify.com/v1';
export enum Endpoint {
  Me = '/me',
  CurrentUserPlaylists = '/me/playlists',
  Playlist = '/playlists/:id',
  StartOrResume = '/me/player/play',
}
export const endpoint = (endpoint: Endpoint) => baseApiUrl + endpoint;

const PAGE_SIZE = 30;

const spotifyHelper = {
  baseAuthorizationUrl,
  endpoint,
  Endpoint,
  PAGE_SIZE,
};

export default spotifyHelper;
