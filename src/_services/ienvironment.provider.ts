export default interface IEnvironmentProvider {
  getScope(): string;
  getIsProd(): boolean;

  getScopes(): string[];
  getClientInfo: () => { clientId: string; clientSecret: string };
}
