export enum WebPlaybackErrorEnum {
  Playback = 'playback_error',
  Account = 'account_error',
  Authentication = 'authentication_error',
  Initialization = 'initialization_error',
}
