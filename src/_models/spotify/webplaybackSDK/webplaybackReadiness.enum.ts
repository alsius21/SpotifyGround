export enum WebPlaybackReadinessEnum {
  Ready = 'ready',
  NotReady = 'not_ready',
}
