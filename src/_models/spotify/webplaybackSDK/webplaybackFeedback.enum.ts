export enum WebPlaybackFeedbackEnum {
  PlayerStateChanged = 'player_state_changed',
  AutoPlayFailed = 'autoplay_failed',
}
