type ImageSize = 'UNKNOWN' | 'SMALL' | 'LARGE';

export type WebPlaybackAlbumImageObject = {
  url: string; // "https://image/xxxx"
  size: ImageSize;
};

type WebPlaybackTrackContentType =
  | 'track'
  | 'episode'
  | 'ad'
  | 'TRACK'
  | 'EPISODE'
  | 'AD';

type WebPlaybackTrackMediaType = 'audio' | 'video';

export type WebPlaybackArtistObject = {
  uri: string; // 'spotify:artist:xxxx'
  name: string; // "Artist Name"
};

// https://developer.spotify.com/documentation/web-playback-sdk/reference/#object-web-playback-state
export type WebPlaybackTrackObject = {
  uri: string; // "spotify:track:xxxx" - Spotify URI
  id: string; // "xxxx" -  Spotify ID from URI (can be null)
  uid: string; //  "xxxx" - To identify the same song, it does not change
  type: WebPlaybackTrackContentType; // Content type: can be "track", "episode" or "ad"
  media_type: WebPlaybackTrackMediaType; // Type of file: can be "audio" or "video"
  name: string; // "Song Name" - Name of content
  is_playable: boolean; // Flag indicating whether it can be played
  album: {
    uri: string; // Spotify Album URI 'spotify:album:xxxx'
    name: string; // Album Name
    images: WebPlaybackAlbumImageObject[];
  };
  artists: WebPlaybackArtistObject[];
};

export enum RepeatModeType {
  NoRepeat,
  Song,
  Playlist,
}

export type WebPlaybackStateObject = {
  context: {
    uri?: string; // 'spotify:album:xxx' The URI of the context (can be null)
    metadata?: Record<string, unknown>; // Additional metadata for the context (can be null)
  };
  disallows: {
    // A simplified set of restriction controls for
    pausing: false; // The current track. By default, these fields
    peeking_next?: false; // will either be set to false or undefined, which
    peeking_prev: false; // indicates that the particular operation is
    resuming: false; // allowed. When the field is set to `true`, this
    seeking: false; // means that the operation is not permitted. For
    skipping_next: false; // example, `skipping_next`, `skipping_prev` and
    skipping_prev: false; // `seeking` will be set to `true` when playing an ad track.
    toggling_repeat_context: false;
    toggling_repeat_track: false;
    toggling_shuffle: false;
  };
  paused: boolean; // Whether the current track is paused.
  position: number; // The position_ms of the current track.
  repeat_mode: RepeatModeType;
  shuffle: boolean; // True if shuffled, false otherwise.
  track_window: {
    current_track: WebPlaybackTrackObject; // The track currently on local playback
    previous_tracks: WebPlaybackTrackObject[]; // Previously played tracks. Number can vary.
    next_tracks: WebPlaybackTrackObject[]; // Tracks queued next. Number can vary.
  };
};

export type WebPlaybackErrorObject = {
  message: string;
};

export interface PlayerStateChangedResponse extends WebPlaybackStateObject {
  duration: number;
  timestamp: number;
  playback_quality: 'VERY_HIGH';
  playback_id: string;
  playback_features: {
    hifi_status: 'NONE';
    playback_speed: {
      current: 1;
      selected: 1;
      restricted: boolean;
    };
  };
  loading: false; // Flag indicating whether the player is currently loading a new track
  restrictions: {
    disallow_seeking_reasons: [];
    disallow_skipping_next_reasons: [];
    disallow_resuming_reasons: 'not_paused'[];
    disallow_skipping_prev_reasons: 'no_prev_track'[];
    disallow_toggling_repeat_context_reasons: [];
    disallow_toggling_repeat_track_reasons: [];
    disallow_toggling_shuffle_reasons: [];
    disallow_peeking_next_reasons: [];
    disallow_peeking_prev_reasons: [];
  };
  playback_speed: 1;
}
