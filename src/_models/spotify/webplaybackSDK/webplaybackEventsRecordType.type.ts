import { PlayerStateChangedResponse } from './webplayback.model';
import { WebPlaybackErrorEnum } from './webplaybackError.enum';
import { WebPlaybackFeedbackEnum } from './webplaybackFeedback.enum';
import { WebPlaybackReadinessEnum } from './webplaybackReadiness.enum';
export type EventMessage = {
  message: string;
};

export type EventDevice = {
  device_id: string;
};

export type WebplaybackEventsRecordType = {
  [WebPlaybackErrorEnum.Account]: (args: EventMessage) => void;
  [WebPlaybackErrorEnum.Authentication]: (args: EventMessage) => void;
  [WebPlaybackErrorEnum.Initialization]: (args: EventMessage) => void;
  [WebPlaybackErrorEnum.Playback]: (args: EventMessage) => void;
  [WebPlaybackReadinessEnum.Ready]: (args: EventDevice) => void;
  [WebPlaybackReadinessEnum.NotReady]: (args: EventDevice) => void;
  [WebPlaybackFeedbackEnum.PlayerStateChanged]: (
    args: PlayerStateChangedResponse,
  ) => void;
  [WebPlaybackFeedbackEnum.AutoPlayFailed]: () => void;
};
