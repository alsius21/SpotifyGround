'use client';
import shortenArtists from './shortenArtists';
import { Card } from '@/_components/Card';
import toast from 'react-hot-toast';
import { service } from '@/_services/spotify-sdk.service';
import { cn } from '@/_utilities/cn';

const max25Chars = (text: string): string => {
  if (text.length > 28) {
    return text.slice(0, 28) + '...';
  }
  return text;
};

export interface Props {
  images: { width?: number; height?: number; url: string }[];
  artists: string[];
  name: string;
  onClick: (device_id: string | undefined, uri: string) => Promise<void>;
  active?: boolean;
  uri: string;
}

/**
 * The cover of a song, its info required by spotify and provides some interactivity, such to play pause
 */
export const Song = ({
  artists,
  name,
  onClick,
  images,
  uri,
  active,
}: Props): JSX.Element => {
  return (
    <button
      data-active={active}
      className={cn(
        'song cursor-pointer w-full hover:opacity-60 active:opacity-80 focus:opacity-70',
        active && 'border',
      )}
      onClick={() => {
        console.log('click:song:' + name);
        onClick(service.device_id, uri).then(() => {
          toast.success('Playing ' + name);
        });
      }}
    >
      <Card images={images}>
        <div className="song flex flex-col text-sm lg:text-base text-center md:text-start flex-grow p-4">
          <p className="song__title">{max25Chars(name)}</p>
          <p className="song__artists text-gray-600 text-xs">
            {shortenArtists(artists)}
          </p>
        </div>
      </Card>
    </button>
  );
};
