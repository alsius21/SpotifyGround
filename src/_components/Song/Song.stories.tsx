import { Song, Props } from '@/_components/Song/index';
import { Meta, StoryFn } from '@storybook/react';

export default {
  title: '@/_components/Song',
  component: Song,
  args: {
    onDoubleClickCovers: () => alert('double'),
    onClick: () => alert('single'),
  },
} as Meta;

const Template: StoryFn<Props> = (args: Props) => <Song {...args} />;

export const WithImage = Template.bind({});

const args: Props = {
  artists: ['Disclosure', 'Flume'],
  uri: 'spotify:track:3U4isOIWM3VvDubwSI3y7a',
  onClick: () => {
    alert('single');
    return Promise.resolve();
  },
  images: [
    {
      url: 'https://i.scdn.co/image/ab67616d0000b27379979c326365ecf6db1a844c',
    },
  ],
  name: 'Tondo',
};

WithImage.args = { ...args };

export const WithoutImage = Template.bind({});
WithoutImage.args = {
  ...args,
  images: [],
};
