import cx from 'clsx';
const shortenArtist = (artist: string) =>
  artist.length > 24 ? artist.slice(0, 21).concat('...') : artist;
export const shortenArtists = (artists: string[]): string =>
  shortenArtist(artists?.reduce((a, b) => cx(a, b), ''));
export default shortenArtists;
