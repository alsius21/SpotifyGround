import styles from './Button.module.css';

import cx from 'clsx';

type Presets = 'pill' | 'square' | 'none';
type Devices = 'mobile' | 'desktop' | 'all';
type Places = 'center';

type PartialButtonHTMLAttributes = Pick<
  React.ButtonHTMLAttributes<HTMLButtonElement>,
  'onClick' | 'className' | 'children' | 'disabled' | 'type'
>;

export interface ButtonProps extends PartialButtonHTMLAttributes {
  margin?: 'top' | 'bottom';
  preset: Presets;
  device?: Devices;
  place?: Places;
}

const Margin = {
  top: 'mt-4',
  bottom: 'mb-4',
};

const Place = {
  center: cx(styles['standalone--center']),
};

const Preset: { [key in Presets]: string } = {
  square: cx(
    'bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded',
  ),
  pill: cx(
    'bg-purple-500 hover:bg-purple-700 text-white font-bold py-2 px-12 rounded-full text-center',
    'align-center ml-auto mr-auto flex',
  ),
  none: '',
};

const Visibility: { [key in Devices]: string } = {
  mobile: 'sm:hidden',
  desktop: 'hidden sm:block',
  all: '',
};

export const Button = (props: ButtonProps): JSX.Element => {
  const {
    margin,
    preset,
    device,
    place,
    className,
    onClick,
    children,
    ...buttonProps
  } = props;

  const buttonClassName = cx(
    'Button',
    margin && Margin[margin],
    Preset[preset],
    className,
    device && Visibility[device],
    place && Place[place],
  );

  const updatedProps: React.ButtonHTMLAttributes<HTMLButtonElement> = {
    ...buttonProps,
    className: buttonClassName,
    onClick,
  };

  return <button {...updatedProps}>{children}</button>;
};
