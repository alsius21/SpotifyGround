import { Button } from './Button';
import { render } from '@testing-library/react';
describe('Button', () => {
  it('should render', () => {
    const { getByText } = render(<Button preset="square">Test</Button>);
    expect(getByText('Test')).toBeInTheDocument();
  });
});
