import { Meta, StoryFn } from '@storybook/react';
import { Button, ButtonProps } from './Button';

const defaultStoryProps: ButtonProps = {
  children: 'Jo vaig a dalt',
  preset: 'square',
  onClick: () => {
    console.log('default button');
  },
};

export default {
  title: '@/_components/Button',
  component: Button,
  args: defaultStoryProps,
} as Meta;

const Template: StoryFn<ButtonProps> = (args: ButtonProps) => (
  <Button {...args} />
);

export const DefaultButton = Template.bind({});
export const PillButton = Template.bind({});

PillButton.args = {
  preset: 'pill',
};
