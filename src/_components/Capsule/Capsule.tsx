import { cn } from '@/_utilities/cn';
import cx from 'clsx';
import { createElement } from 'react';

export interface CapsuleProps {
  children: React.ReactNode;
  className?: string;
  innerClassName?: string;
  id?: string;
  outerTag?:
    | 'section'
    | 'div'
    | 'article'
    | 'aside'
    | 'header'
    | 'footer'
    | 'main';
}

export const Capsule = (props: CapsuleProps): JSX.Element => {
  const outerProps = {
    className: cn('px-4', props.className),
    id: props.id ?? '',
  };

  const innerChildren = (
    <div
      className={cx('capsule__inner mx-auto ', props.innerClassName)}
      id={props.id}
    >
      {props.children}
    </div>
  );
  return createElement(props.outerTag ?? 'section', outerProps, innerChildren);
};
