import { Capsule, CapsuleProps } from './Capsule';
import { Meta, StoryFn } from '@storybook/react';

export default {
  title: '@/_components/Capsule',
  component: Capsule,
} as Meta;

const Template: StoryFn<CapsuleProps> = (args: CapsuleProps) => (
  <Capsule className={args.className} innerClassName={args.innerClassName}>
    {args.children}
  </Capsule>
);

const insideCapsule: JSX.Element = <div>I am inside a capsule</div>;

const capsuleProps: CapsuleProps = {
  innerClassName: '',
  className: '',
  children: insideCapsule,
};

export const DefaultCapsule = Template.bind({});
DefaultCapsule.args = capsuleProps;
