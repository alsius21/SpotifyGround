import { msToHHMMSS } from '@/_services/time.service';
type Props = {
  position: number;
  duration: number;
};

const ProgressTime = ({ position, duration }: Props) => {
  return (
    <div className="ellasped-time-container md:flex gap-x-1 justify-center hidden">
      <div>{msToHHMMSS(position)}</div>
      <div>{msToHHMMSS(duration)}</div>
    </div>
  );
};
export default ProgressTime;
