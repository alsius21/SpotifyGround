import CenterLayout from './CenterLayout';
import { render } from '@testing-library/react';

describe('CenterLayout', () => {
  it('should render', () => {
    const { queryByTestId } = render(<CenterLayout />);

    expect(queryByTestId('center-route')).toBeInTheDocument();
  });
});
