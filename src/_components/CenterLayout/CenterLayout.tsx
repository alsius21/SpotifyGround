import { Layout } from '@/_components/Layout';
import cx from 'clsx';

const CenterLayout = ({ children }: { children?: React.ReactNode }) => {
  return (
    <Layout centered data-testid="center-route">
      <header className="fixed w-full" />
      {children}
      <footer className={cx('w-full h-32 fixed bottom-0')} />
    </Layout>
  );
};
export default CenterLayout;
