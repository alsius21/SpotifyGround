import { cn } from '@/_utilities/index';

function Skeleton({
  className,
  ...props
}: React.HTMLAttributes<HTMLDivElement>) {
  return <div className={cn('rounded-md bg-white', className)} {...props} />;
}

export { Skeleton };
