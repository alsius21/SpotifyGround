import { Meta, StoryFn } from '@storybook/react';
import { MemoryRouter } from 'react-router-dom';
import TopBar, { Props } from './TopBar';

const defaultArgs: Props = {
  label: '',
  imageUrl: '/playlist/cover.jpg',
  buttonText: 'header:buttonText',
  exitText: 'header:exitText',
};

export default {
  title: '@/_components/TopBar',
  component: TopBar,
  args: defaultArgs,
  decorators: [
    (Story) => (
      <MemoryRouter>
        <Story />
      </MemoryRouter>
    ),
  ],
} as Meta;

const Template: StoryFn<Props> = (args: Props) => <TopBar {...args} />;

export const HeaderWithImage = Template.bind({});

export const HeaderLogin = Template.bind({
  label: 'login',
});
