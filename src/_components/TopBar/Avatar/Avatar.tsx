type Props = {
  imageUrl: string;
};

const Avatar = ({ imageUrl }: Props): JSX.Element => (
  <img
    src={imageUrl}
    className=" h-10 w-10 rounded-full"
    alt={'user'}
    title={'user'}
  />
);

export default Avatar;
