import TopBar from './TopBar';
import { render } from '@testing-library/react';

jest.mock('./LanguageSelection', () => ({
  __esModule: true,
  default: jest.fn(),
}));

jest.mock('next-i18next', () => ({
  useTranslation: jest.fn().mockImplementation(() => ({
    t: jest.fn().mockImplementation((key) => key),
  })),
}));
jest.mock('react-router-dom', () => {
  return {
    Link: jest.fn().mockImplementation(({ children }) => children),
  };
});

describe('TopBar', () => {
  it('should render', () => {
    const label = 'Home';
    const { getByText } = render(<TopBar label={label} />);
    expect(getByText(label)).toBeInTheDocument();
    expect(
      getByText('header:buttonText', { exact: false }),
    ).toBeInTheDocument();
    expect(getByText('header:exitText', { exact: false })).toBeInTheDocument();
  });
});
