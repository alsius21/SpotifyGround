'use client';
import { useRouter } from 'next/navigation';
import { useTranslation } from 'react-i18next';

export default function LanguageSelection() {
  const { i18n } = useTranslation();
  const router = useRouter();
  return (
    <select
      onChange={(...args) => {
        router.push(`/${args[0].target.value}`);
      }}
      value={i18n.language}
      className="appearance-none inline-block text-sm px-4 py-2 leading-none rounded text-white cursor-pointer hover:border-transparent hover:text-indigo-500 hover:bg-white lg:mt-0 antialiased font-semibold bg-transparent"
    >
      <option value="en">English</option>
      <option value="es">Español</option>
      <option value="ca">Català</option>
    </select>
  );
}
