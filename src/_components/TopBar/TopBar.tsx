import cx from 'clsx';
import Link from 'next/link';
import Avatar from './Avatar';
import RoutePath from '@/app/routePathEnum';
import LanguageSelection from './LanguageSelection';
import { Capsule } from '@/_components/Capsule';

interface Props {
  imageUrl?: string;
  label: string;
  buttonText: string;
  exitText: string;
}

function TopBar(props: Readonly<Props>) {
  const {
    imageUrl = '/playlist/cover.jpg',
    label = '',
    buttonText = 'header:buttonText',
    exitText = 'header:exitText',
  } = props;

  return (
    <Capsule
      outerTag="header"
      innerClassName="flex items-center sm:justify-between justify-center flex-wrap py-4 gap-4 sm:gap-0"
    >
      <Link
        href={RoutePath.CLUB}
        className="flex items-center flex-shrink-0 text-white gap-2 cursor-pointer"
      >
        <div className="">
          <Avatar imageUrl={imageUrl} />
        </div>
        <span className={cx('tracking-tight')}>
          <span className="font-normal">{label}</span>
        </span>
      </Link>
      <div className="flex gap-x-2">
        <Link
          aria-label="to-profile"
          href={RoutePath.DRESSROOM}
          className="order-1 sm:order-none inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-indigo-500 hover:bg-white lg:mt-0 antialiased font-semibold"
        >
          <span>{buttonText}</span>
        </Link>
        <div>
          <LanguageSelection />
        </div>
        <Link
          aria-label="exit"
          href={RoutePath.EXIT}
          className="order-2 sm:order-none inline-block text-sm px-4 py-2 leading-none rounded text-whitelg:mt-0 antialiased font-semibold"
        >
          <span>{'🚪 '}</span>
          <span>{exitText}</span>
        </Link>
      </div>
    </Capsule>
  );
}

export default TopBar;
export type { Props };
