import { render } from '@testing-library/react';
import { Pagination } from './Pagination';

jest.mock('./PageSelector', () => ({ PageSelector: jest.fn() }));

describe('Pagination', () => {
  it('renders without crashing', () => {
    // Given
    const props = {
      total: 5,
      currentPage: 1,
      onSelectPage: jest.fn(),
    };
    // When
    const { container } = render(<Pagination {...props} />);
    // Then
    expect(container).toBeInTheDocument();
  });
});
