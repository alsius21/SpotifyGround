import cx from 'clsx';
import Link from 'next/link';
export interface Props {
  selected?: boolean;
  children: React.ReactNode;
  href: string;
}

export function PageSelector(props: Props): JSX.Element {
  const { selected = false, children, href } = props;
  return (
    <Link
      href={href}
      className={cx(selected && 'font-bold text-xl')}
      aria-selected={selected}
    >
      {children}
    </Link>
  );
}
