'use client';

import { PageSelector } from './PageSelector';
import spotifyHelper from '@/_services/spotify.helper';

export interface Props {
  total: number;
  currentPage: number;
  baseHref: string;
}

/**
 *
 * @param props {Props}
 * @returns JSX.Element
 */
export function Pagination(props: Readonly<Props>): JSX.Element {
  const { total, currentPage, baseHref } = props;
  const pages: number[] = [];
  for (let i = 0; i < (total ?? 1); i++) {
    pages.push(i);
  }

  return (
    <div className="flex justify-center items-center gap-x-4 text-center flex-wrap">
      {pages.map((i) => (
        <PageSelector
          key={i}
          selected={i + 1 === currentPage}
          href={`${baseHref}${i * spotifyHelper.PAGE_SIZE}`}
        >
          {i + 1}
        </PageSelector>
      ))}
    </div>
  );
}
