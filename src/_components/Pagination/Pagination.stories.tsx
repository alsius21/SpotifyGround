import { Meta, StoryFn } from '@storybook/react';
import { withRouter } from 'storybook-addon-remix-react-router';

import { Pagination, Props } from './Pagination';

export default {
  title: '@/_components/Pagination',
  component: Pagination,
  decorators: [withRouter],
  args: {
    currentPage: 0,
    total: 0,
  } as Props,
} as Meta;

const Template: StoryFn<Props> = (args: Props) => <Pagination {...args} />;

export const Default = Template.bind({});

export const DoublePage = Template.bind({});
DoublePage.args = {
  total: 2,
  currentPage: 1,
};

export const MultiplePage = Template.bind({});
MultiplePage.args = {
  total: 20,
  currentPage: 2,
};
