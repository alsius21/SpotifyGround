'use client';
import React from 'react';

import { SessionProvider } from 'next-auth/react';

interface Props {
  children: React.ReactNode;
}

function AuthProvider({ children }: Readonly<Props>) {
  return <SessionProvider>{children}</SessionProvider>;
}

export default AuthProvider;
