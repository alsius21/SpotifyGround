import Link from 'next/link';

export type TitleProps = {
  text: string;
  description: string;
  url: string;
};

const Title: React.FC<TitleProps> = (props: TitleProps) => {
  return (
    <div id="Title">
      <h1 className="text-5xl font-bold text-center">{props.text}</h1>
      <Link
        className="animate-pulse hover: cursor-pointer ml-auto mr-auto flex justify-center"
        href={props.url}
      >
        <span>{props.description}</span>
      </Link>
    </div>
  );
};

export default Title;
