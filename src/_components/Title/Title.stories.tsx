import Title, { TitleProps } from './Title';
import { Meta, StoryFn } from '@storybook/react';

export default {
  title: 'Welcome/Title',
  component: Title,
} as Meta;

const Template: StoryFn<TitleProps> = (args: TitleProps) => <Title {...args} />;

export const Default = Template.bind({});
Default.args = {
  description: 'description',
  text: 'Tested title',
};
