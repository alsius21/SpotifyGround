import Title, { TitleProps } from './Title';
import { render } from '@testing-library/react';

describe('Title', () => {
  it('should render', () => {
    const props: TitleProps = {
      text: 'Welcome',
      description: 'ClickTo Continue',
      onClick: jest.fn(),
    };
    const { getByText } = render(
      <Title
        text={props.text}
        description={props.description}
        onClick={props.onClick}
      />,
    );
    expect(getByText(props.text)).toBeInTheDocument();
  });
});
