import { Navigate, useRouteError } from 'react-router-dom';
import RoutePath from '@/app/routePathEnum';
const ErrorElement = ({ log }: { log: string }) => {
  const error = useRouteError();
  console.debug(
    `errorElement::Something broke the page status at ${log}`,
    error,
  );
  return <Navigate to={RoutePath.HOME} />;
};

export default ErrorElement;
