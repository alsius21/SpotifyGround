import { Card } from './Card';
import { render } from '@testing-library/react';
describe('Card', () => {
  it('should render', () => {
    const props = {
      imgSources: [
        'https://example.com/image.jpg',
        'https://example.com/image2.jpg',
      ],
      children: <div>Test</div>,
    };
    const { queryByText } = render(
      <Card imgSources={props.imgSources}>{props.children}</Card>,
    );
    expect(queryByText('Test')).toBeInTheDocument();
  });
});
