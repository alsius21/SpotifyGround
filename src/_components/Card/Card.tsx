import Image from 'next/image';

interface Props {
  images: { url: string; width?: number; height?: number }[];
  children: JSX.Element | Array<JSX.Element>;
}

export const Card = (props: Props) => {
  const hasImage = props.images.length > 0;

  return (
    <div className="card flex flex-wrap gap-2 justify-center">
      <div className="card__image w-16 h-16 lg:w-20 lg:h-20 cursor-pointer flex-shrink-0">
        {hasImage && (
          <Image
            height={props.images[0].height}
            width={props.images[0].width}
            src={props.images[0].url}
            loading="lazy"
            alt="cover"
          />
        )}
        {!hasImage && (
          <div>
            <div className="w-16 h-16 lg:w-20 lg:h-20 bg-blue-900 flex items-center justify-center" />
          </div>
        )}
      </div>
      {props.children}
    </div>
  );
};
