import { Layout } from './Layout';
import { render } from '@testing-library/react';

describe('Layout', () => {
  it('should render', () => {
    const testId = 'test';
    const { getByTestId } = render(<Layout data-testid={testId} />);
    expect(getByTestId(testId)).toBeInTheDocument();
  });
});
