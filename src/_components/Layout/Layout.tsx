import React, { FC } from 'react';
import cx from 'clsx';
import css from './Layout.module.css';

type Props = {
  children?: React.ReactNode;
  'data-testid'?: string;
  centered?: boolean;
};

export const Layout: FC<Props> = (props: Props) => (
  <div
    data-testid={props['data-testid']}
    className={cx(
      css.layout,
      'overflow-x-hidden text-white relative w-100 h-100 min-h-screen flex flex-col',
      props.centered && 'justify-center',
      !props.centered && 'justify-between',
    )}
  >
    {props.children}
  </div>
);
