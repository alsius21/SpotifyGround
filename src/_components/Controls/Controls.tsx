import cx from 'clsx';
import { MouseEventHandler } from 'react';

type Props = {
  onClickPlay: () => void;
  disabled: boolean;
  isPlaying: boolean;
};
const Controls = ({ onClickPlay, disabled, isPlaying }: Props) => {
  const handleClick: MouseEventHandler<HTMLButtonElement> = (e) => {
    e.preventDefault();
    e.stopPropagation();
    onClickPlay();
  };
  return (
    <div className="controls-container md:flex gap-x-1 justify-center hidden">
      <button
        id="toggle-play"
        className={cx(!disabled && 'cursor-pointer')}
        onClick={handleClick}
        disabled={disabled}
      >
        {isPlaying ? '⏸️' : '▶️'}
      </button>
    </div>
  );
};

export default Controls;
