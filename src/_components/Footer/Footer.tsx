import cx from 'clsx';

interface Props {
  children?: JSX.Element | JSX.Element[];
}

const Footer = (props: Props): JSX.Element => {
  return <footer className={cx('w-full h-32')}>{props.children}</footer>;
};

export type { Props as FooterProps };
export default Footer;
