import Footer, { FooterProps } from './Footer';
import { Meta, StoryFn } from '@storybook/react';

export default {
  title: 'Footer',
  component: Footer,
} as Meta;

const Template: StoryFn<FooterProps> = (args) => <Footer {...args} />;

export const Bottom = Template.bind({});
Bottom.args = {
  children: undefined,
};
