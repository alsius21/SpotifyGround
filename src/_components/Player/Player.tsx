'use client';
import { PlayerContext } from '@/app/(auth)/club/_script/client-setup';
import DigitalPlayer from './components/digital.player';
import { useContext } from 'react';

/**
 * This component displays the current track and allow the user to play/pause it.
 */
const Player = (): JSX.Element => {
  const playback_id = '';
  const lastState = useContext(PlayerContext);

  const {
    name = '',
    artists = [],
    album = { images: [] },
  } = lastState?.track_window.current_track ?? {};
  const list = lastState?.context.uri ?? '';
  const trackImage = album.images[0]?.url ?? '';
  return (
    <section
      key={playback_id}
      id="player"
      className="p-4 flex flex-col gap-y-2 bg-spotify-dark rounded-sm shadow-2xl "
    >
      <DigitalPlayer
        trackName={name}
        artists={artists.map((artist) => artist.name).join(', ')}
        activeIds={{ list }}
        trackImage={trackImage}
        durationMs={lastState?.duration ?? 0}
      />
    </section>
  );
};

export default Player;
