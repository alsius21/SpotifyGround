'use client';
import { Copy } from 'lucide-react';

import {
  Cover,
  Info,
  CoverSkeleton,
  InfoSkeleton,
  type InfoProps,
  type CoverProps,
} from './index';
import { usePlayerControls, useProgressBarForwarding } from '../hooks';

import withPlaceholder, {
  type WithPlaceholderProps,
} from '@/_hoc/withPlaceholder';
import { ProgressBar } from '@/_components/ProgressBar';
import toast from 'react-hot-toast';

const Information = withPlaceholder<InfoProps & WithPlaceholderProps>(
  Info,
  InfoSkeleton,
);
const TrackCover = withPlaceholder<CoverProps & WithPlaceholderProps>(
  Cover,
  CoverSkeleton,
);

interface Props {
  trackName: string;
  artists: string;
  activeIds: { list: string };
  trackImage: string;
  durationMs: number;
}

const DigitalPlayer = ({
  trackName,
  artists,
  activeIds,
  trackImage,
  durationMs,
}: Props) => {
  const loaded = !!trackName;
  const [pos, isPlaying] = useProgressBarForwarding();
  const { togglePlay } = usePlayerControls();

  const onClickPlay = loaded ? () => togglePlay() : () => toast.error('💩');

  const { play: isPlayDisallowed, pause: isPauseDisallowed } = {
    play: false,
    pause: false,
  };

  const isPlayButtonEnabled =
    (isPlaying && !isPauseDisallowed) || (!isPlaying && !isPlayDisallowed);

  const position = loaded ? pos : 0;
  const duration = loaded ? durationMs : 0;

  return (
    <>
      <div className="flex flex-col flex-grow gap-4" id="player-details">
        <div
          id="on_air-info"
          className="whitespace-nowrap flex gap-x-2 overflow-clip"
        >
          <button
            onClick={onClickPlay}
            disabled={!isPlayButtonEnabled}
            className="hover:opacity-50"
          >
            <TrackCover src={trackImage} ready={loaded && !!trackImage} />
          </button>
          <Information
            trackName={trackName}
            artistsText={artists}
            ready={loaded}
          />
          {loaded && (
            <button
              className="h-4"
              onClick={() => {
                if (!activeIds.list) return;
                navigator.clipboard.writeText(activeIds.list.split(':')[2]);
                toast.success('Copied to clipboard');
              }}
            >
              <Copy className="w-4" />
            </button>
          )}
        </div>
        <div className="flex justify-center">
          <div className="flex flex-1">
            <ProgressBar value={position} max={duration} paused={!isPlaying} />
          </div>
        </div>
      </div>
      <div className="flex flex-row gap-4 items-center justify-end">
        <p className="text-sm">by Spotify</p>
        <div className="w-6 md:w-8">
          <img
            src="/images/spotify_logo-simple--green.png"
            alt="Spotify Logo"
          />
        </div>
      </div>
    </>
  );
};

export default DigitalPlayer;
