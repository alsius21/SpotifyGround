import cx from 'clsx';
import { MouseEventHandler } from 'react';

type Props = React.ButtonHTMLAttributes<HTMLButtonElement> & {
  isPlaying: boolean;
};
export function TogglePlayButton(props: Props): JSX.Element {
  const { onClick = () => {}, disabled, isPlaying } = props;
  const handleClick: MouseEventHandler<HTMLButtonElement> = (e) => {
    e.preventDefault();
    e.stopPropagation();
    onClick(e);
  };
  return (
    <button
      className={cx(!disabled && 'cursor-pointer')}
      onClick={handleClick}
      disabled={disabled}
    >
      {isPlaying ? '⏸️' : '▶️'}
    </button>
  );
}
