import cx from 'clsx';
import { Skeleton } from '@/_components/ui/skeleton';
export type Props = { trackName: string; artistsText: string };

export const Info = ({ trackName, artistsText }: Props): React.ReactNode => {
  return (
    <div className="md:flex md:flex-col">
      <span>{trackName}</span>
      <span className="md:hidden"> - </span>
      <span className={cx(' text-gray-600')}>{artistsText}</span>
    </div>
  );
};

export const InfoSkeleton = () => (
  <>
    <Skeleton className="h-4 w-2/3" />
    <Skeleton className="h-4 w-1/5 bg-gray-600" />
  </>
);
