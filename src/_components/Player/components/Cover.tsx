import { Skeleton } from '@/_components/ui/skeleton';
export interface Props {
  src: string;
}
export const Cover = ({ src }: Props) => {
  return (
    <div className="info__image w-6 h-6 cursor-pointer flex-shrink-0 md:w-32 md:h-32">
      <img src={src} alt="cover_for_current_song" loading="lazy" />
    </div>
  );
};

export const CoverSkeleton = () => <Skeleton className="w-4 h-4" />;
