'use client';
import { PlayerContext } from '@/app/(auth)/club/_script/client-setup';
import { useContext, useEffect, useState } from 'react';

const FORWARD_MS = 1000;

let id: NodeJS.Timeout;
export default function useProgressBarForwarding() {
  const lastState = useContext(PlayerContext);
  const playing = lastState && !lastState.paused;
  const lastStatePosition = lastState?.position ?? 0;
  const playbackId = lastState ? lastState.playback_id : '';
  const [position, setPosition] = useState(lastStatePosition || 0);
  // progressbar polling
  useEffect(() => {
    if (playing) {
      setPosition(lastStatePosition);
      id && clearInterval(id);
      id = setInterval(() => {
        if (playing) {
          setPosition((prev) => prev + FORWARD_MS);
        }
      }, FORWARD_MS);
    }
    return () => {
      id && clearInterval(id);
    };
  }, [playing, lastStatePosition, playbackId]);

  return [position, playing] as const;
}
