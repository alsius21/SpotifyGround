import { service as webplaybackService } from '@/_services/spotify-sdk.service';
import spotifyApiService from '@/_services/spotify-api';
import RoutePath from '@/app/routePathEnum';

import toast from 'react-hot-toast';
import { redirect } from 'next/navigation';

export type PlayerControls = {
  togglePlay: () => Promise<void>;
  goToPlaylist: () => void;
  goToTrackPosition: (position: number) => void;
};

export default function usePlayerControls(): PlayerControls {
  const playlistId = '';
  const deviceId = '';
  const paused = true;
  const goToTrackPosition = (position: number) => {
    webplaybackService.seek(Number(position));
  };
  const goToPlaylist = () => {
    if (!playlistId) {
      console.warn('No playlistId found');
      return;
    }
    if (!window.location.pathname.includes(playlistId)) {
      redirect(RoutePath.MIXTAPE.replace(':id', playlistId));
    }
  };

  const togglePlay = async () => {
    const action = paused ? 'play' : 'pause';
    if (!deviceId) {
      toast.error('Player is not connected');
      return;
    }
    console.log('togglePlay::%s', action, deviceId);
    try {
      await spotifyApiService[action](deviceId);
    } catch (e) {
      console.error(e);
    }
  };

  return {
    goToTrackPosition,
    goToPlaylist,
    togglePlay,
  };
}
