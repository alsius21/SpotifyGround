import { Button } from '../Button';
import {
  StepInformation,
  StepInformationProps,
} from '@/_components/StepInformation/StepInformation';
import { StoryFn } from '@storybook/react';

export default {
  title: 'Welcome/StepInformation',
  component: StepInformation,
};

const Template: StoryFn<typeof StepInformation> = (
  args: StepInformationProps,
) => <StepInformation {...args} />;

export const Hello = Template.bind({});
Hello.args = {
  title: 'Aquí va el títol',
  description: 'Aquí va una descripcio tota maca',
  children: (
    <>
      <Button
        place="center"
        className="mb-3"
        preset="square"
        onClick={() => {
          console.log('top');
        }}
      >
        {'Jo vaig a dalt'}
      </Button>
      <Button
        place="center"
        preset="square"
        onClick={() => {
          console.log('bottom');
        }}
      >
        {'I jo a baix'}
      </Button>
    </>
  ),
};
