import cx from 'clsx';

export interface StepInformationProps {
  title: string;
  description: string;
  children: JSX.Element;
}

export const StepInformation = (props: StepInformationProps): JSX.Element => {
  return (
    <>
      <h2 className={'text-center'}>{props.title}</h2>
      <p className={cx('mb-4 sm: px-2 text-center')}>{props.description}</p>
      {props.children}
    </>
  );
};
