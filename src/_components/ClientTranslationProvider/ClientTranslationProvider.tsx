import 'src/_services/i18n/client';

const ClientTranslationProvider: React.FC = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  return <>{children}</>;
};

export default ClientTranslationProvider;
