import cx from 'clsx';
import styles from './ProgressBar.module.css';
import tailstyles from './ProgressBar.tailwind.module.css';

// Reproductor: Player
export interface ProgressBarProps {
  max: number;
  value: number;
  paused: boolean;
}

export const ProgressBar = (props: ProgressBarProps): JSX.Element => {
  const { value, paused, max } = props;
  const progressRatio = (value / max) * 100;
  const progressColor = paused ? 'lightgray' : '#4370ae';
  const barColor = 'white';
  const backgroundImage = `linear-gradient(90deg, ${progressColor} ${progressRatio}%, ${barColor} ${progressRatio}%)`;

  return (
    <input
      className={cx(
        'ProgressBar__bar h-2 rounded-xl cursor-pointer appearance-none focus:outline-none w-full',
        styles.ProgressBar__bar,
        tailstyles.ProgressBar__bar,
      )}
      style={{
        backgroundImage,
      }}
      value={value}
      type="range"
      id="ellapsedtime"
      name="ellapsedtime"
      disabled
      min={0}
      max={props.max}
      readOnly
    ></input>
  );
};
