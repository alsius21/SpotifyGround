import { Meta, StoryFn } from '@storybook/react';

import { ProgressBar, ProgressBarProps } from './ProgressBar';

export default {
  title: 'Player/ProgressBar',
  component: ProgressBar,
  args: {
    max: 2342342342,
    onProgressChange: () => console.log('progress bar was clicked'),
  },
} as Meta;

const Template: StoryFn<ProgressBarProps> = (args: ProgressBarProps) => (
  <ProgressBar paused={false} max={args.max} value={40} />
);

export const OnBeginBar = Template.bind({});
