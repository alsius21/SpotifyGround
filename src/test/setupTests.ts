import '@testing-library/jest-dom';

const oldWindowLocation = window.location;

// Establish API mocking before all tests.
beforeAll(() => {
  window.location = Object.defineProperties(
    {},
    {
      ...Object.getOwnPropertyDescriptors(oldWindowLocation),
      assign: {
        configurable: true,
        value: jest.fn(),
      },
    },
  ) as Location;
});

// Clean up after the tests are finished.
afterAll(() => {
  window.location = oldWindowLocation;
});
