import React from 'react';
import { render } from '@testing-library/react';
import '../../.storybook/i18n';

const AllTheProviders = ({ children }: { children: React.ReactNode }) => {
  return children;
};

const customRender = (
  ui: Parameters<typeof render>[0],
  options?: Parameters<typeof render>[1],
) => render(ui, { wrapper: AllTheProviders, ...options });

// re-export everything
export * from '@testing-library/react';

// override render method
export { customRender as render };
