export type WithPlaceholderProps = {
  ready: boolean;
};

const withPlaceholder = <T extends WithPlaceholderProps>(
  BaseComponent: React.ComponentType<T>,
  Placeholder: React.ComponentType<unknown>,
) => {
  const WithPlaceholder = (props: T & WithPlaceholderProps): JSX.Element => {
    const { ready, ...rest } = props;
    return ready ? <BaseComponent {...(rest as T)} /> : <Placeholder />;
  };

  const displayName =
    BaseComponent.displayName || BaseComponent.name || 'Component';

  WithPlaceholder.displayName = `withEnhancer(${displayName})`;

  return WithPlaceholder;
};

export default withPlaceholder;
