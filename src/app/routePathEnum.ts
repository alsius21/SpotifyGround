enum RoutePath {
  CHECK_ID_CARD = '/bouncer/checkIdCard',
  MIXTAPE = '/club/cabin/djsetlists/:id',
  REPERTOIR = '/club/cabin/djsetlists',
  EXIT = '/club/exit',
  ASK_ID_CARD = '/bouncer/askIdCard',
  CABIN = '/club/cabin',
  CLUB = '/club',
  BOUNCER = '/bouncer',
  DANCEFLOOR = '/club/dancefloor',
  DRESSROOM = '/club/dressroom',
  HOME = '/',
}

export default RoutePath;
