import React, { StrictMode } from 'react';
import type { Metadata } from 'next';
import { Raleway } from 'next/font/google';

import { Analytics } from '@vercel/analytics/react';
import { SpeedInsights } from '@vercel/speed-insights/react';
import environmentProvider from '@/_services/environment.provider';
import Script from 'next/script';
import AuthProvider from '@/_components/AuthProvider';
import './style.css';
import { Toaster } from 'react-hot-toast';

export const metadata: Metadata = {
  title: 'No tan vintage',
  description: 'Provide spotify playlists to your relatives and friends.',
};
const raleway = Raleway({
  subsets: ['latin'],
  display: 'swap',
});

interface Props {
  children: React.ReactNode;
  params: { locale: string };
}

export default function RootLayout({
  children,
  params: { locale },
}: Readonly<Props>) {
  return (
    <html lang={locale} className={raleway.className}>
      <head>
        {environmentProvider.getIsProd() && (
          <Script
            async
            src="https://www.googletagmanager.com/gtag/js?id=G-8608PG9LSB"
          />
        )}
        <link rel="manifest" href="/manifest.webmanifest" />
        <link
          rel="icon"
          href="/icon?<generated>"
          type="image/<generated>"
          sizes="<generated>"
        />
        <meta name="theme-color" content="#c9abc9" />
      </head>
      <body>
        <div id="root">
          <StrictMode>
            <AuthProvider>{children}</AuthProvider>
            <Toaster />
          </StrictMode>
          {environmentProvider.getIsProd() && (
            <>
              <SpeedInsights />
              <Analytics />
            </>
          )}
        </div>
        <noscript>You need to enable JavaScript to run this app.</noscript>
      </body>
    </html>
  );
}
