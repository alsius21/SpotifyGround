import cx from 'clsx';

import { Capsule } from '@/_components/Capsule';
import { getCurrentUserProfile } from '@/_services/spotify.serverclient';

export default async function DressRoomPage() {
  const {
    images = [],
    display_name: name,
    href: link,
    followers: { total: followersCount },
    id,
    type: membershipType,
  } = await getCurrentUserProfile();
  const profileImage = images[1];
  return (
    <Capsule innerClassName="card">
      <div className={cx('border-green-600', 'flex p-4 w-auto')}>
        <figure className="mirror-section flex-grow w-10/15">
          <div className="w-40">
            <img src={profileImage.url} alt={name} width={160} height={160} />
          </div>
        </figure>
        <div className="user-description-section flex-grow w-10/15">
          <ul>
            <li className="item">
              <a
                href={link}
                target="_blank"
                rel="noreferrer"
                className="hover:text-green-500 active:text-green-600"
              >
                {name}
              </a>
            </li>
            <li className="item">{followersCount}</li>
            <li className="item">{id}</li>
            <li className="item">{membershipType}</li>
          </ul>
        </div>
      </div>
    </Capsule>
  );
}
