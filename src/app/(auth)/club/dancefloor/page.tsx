import { getPlaylist } from '@/_services/spotify.serverclient';
import Image from 'next/image';

interface Props {
  searchParams: {
    sessionId?: string;
    trackIndex?: string;
  };
}
const DancefloorPage = async ({
  searchParams: { sessionId, trackIndex: rawTrackIndex },
}: Props) => {
  const translations = {
    quiet: 'La pista està tranquila',
  };
  const trackIndex = rawTrackIndex ? parseInt(rawTrackIndex, 10) : 0;
  if (!sessionId) {
    return <h3>{translations.quiet}</h3>;
  }
  const playlist = sessionId ? await getPlaylist(sessionId) : null;
  console.debug('sessionId', sessionId);
  console.debug('trackIndex', trackIndex);
  return (
    <section>
      {playlist && (
        <>
          <h3>{playlist.name}</h3>
          <Image
            className="w-20 h-20"
            width={playlist?.images[0].width}
            height={playlist?.images[0].height}
            src={playlist?.images[0].url}
            alt="something uknown uploaded by the playlist curator or four album covers"
          />
        </>
      )}
    </section>
  );
};

export default DancefloorPage;
