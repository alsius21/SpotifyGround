import React from 'react';
import { Layout } from '@/_components/Layout';
import Footer from '@/_components/Footer';
import { Capsule } from '@/_components/Capsule';
import TopBar from '@/_components/TopBar';
import ClientTranslationProvider from '@/_components/ClientTranslationProvider/ClientTranslationProvider';
import { getCurrentUserProfile } from '@/_services/spotify.serverclient';
import { useTranslation } from '@/_services/i18n';
import { Language, Namespace } from '@/_services/i18n/settings';
import Script from 'next/script';
import ClientSetup from './_script/client-setup';
import { getAccessToken } from '@/_services/authentication-provider';

const ClubLayout = async ({
  children,
  params: { locale },
}: {
  children?: React.ReactNode;

  params: { locale: Language };
}) => {
  const [{ t: tn }, { t }, profile, accessToken] = await Promise.all([
    useTranslation(locale, Namespace.Navigation),
    useTranslation(locale, Namespace.Header),
    getCurrentUserProfile(),
    getAccessToken(),
  ]);
  const images = profile?.images;
  const imageUrl = images ? images[0].url : '';

  return (
    <>
      <Layout>
        <TopBar
          label={tn('welcome')}
          buttonText={t('buttonText')}
          exitText={t('exitText')}
          imageUrl={imageUrl}
        />
        <Capsule
          outerTag="main"
          className="flex-grow"
          innerClassName="flex-1 sm:flex-col sm:flex-row justify-between sm:flex items-stretch gap-y-2 gap-x-2 flex-wrap"
        >
          <ClientSetup accessToken={accessToken}>{children}</ClientSetup>
        </Capsule>
        <Footer />
      </Layout>
      <ClientTranslationProvider />
      <Script src={'https://sdk.scdn.co/spotify-player.js'} async />
    </>
  );
};
export default ClubLayout;
