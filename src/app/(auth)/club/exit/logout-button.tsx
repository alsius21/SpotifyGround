'use client';
import RoutePath from '@/app/routePathEnum';
import { Button } from '@/_components/Button';
import { signOut } from 'next-auth/react';
const LogoutButton = ({ children }: { children: React.ReactNode }) => {
  const cleanAction = () => {
    signOut({ callbackUrl: RoutePath.HOME });
  };
  return (
    <Button
      onClick={cleanAction}
      className={'mb-3 w-48'}
      preset="square"
      place="center"
      margin="top"
    >
      {children}
    </Button>
  );
};
export default LogoutButton;
