import { useTranslation } from '@/_services/i18n';
import RoutePath from '@/app/routePathEnum';
import { Language, Namespace } from '@/_services/i18n/settings';

import Link from 'next/link';
import { Button } from '@/_components/Button';
import LogoutButton from './logout-button';

interface Props {
  params: { locale: Language };
}

export default async function ExitPage({ params }: Props) {
  const { t } = await useTranslation(params.locale, Namespace.Connect);

  return (
    <>
      <Link href={RoutePath.CLUB}>
        <Button className={'mb-3 w-48'} preset="square" place="center">
          {t('loggedIn.next')}
        </Button>
      </Link>
      <LogoutButton>{t('loggedIn.exit')}</LogoutButton>
    </>
  );
}
