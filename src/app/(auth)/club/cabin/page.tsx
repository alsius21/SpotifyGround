import RoutePath from '@/app/routePathEnum';
import Link from 'next/link';

export default function Page() {
  return <Link href={RoutePath.REPERTOIR}>Repertoire</Link>;
}
