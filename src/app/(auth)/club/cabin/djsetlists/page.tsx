import { Capsule } from '@/_components/Capsule';
import RoutePath from '@/app/routePathEnum';
import { Pagination } from '@/_components/Pagination';
import { getCurrentUserPlaylists } from '@/_services/spotify.serverclient';
import cx from 'clsx';
import { GlobeIcon, GlobeLockIcon } from 'lucide-react';
import Link from 'next/link';
import spotifyHelper from '@/_services/spotify.helper';

interface Props {
  searchParams: {
    offset?: string;
  };
}
/**
 * The playlists of the user
 */
export default async function DjSetlistsPage({
  searchParams: { offset },
}: Readonly<Props>) {
  // How many items do I want to request and from which
  const offsetNumber = offset ? parseInt(offset, 10) : 0;
  const playlists = await getCurrentUserPlaylists(offsetNumber);
  const items: Array<{ isPublic: boolean; id: string; name: string }> =
    playlists.items.map((playlist) => ({
      isPublic: playlist.public,
      id: playlist.id,
      name: playlist.name,
    }));
  const total = playlists.total;

  const itemSize = items.length > 1 ? 'small' : 'large';

  return (
    <Capsule
      id="repertoire"
      className="px-0"
      innerClassName="flex flex-col gap-2"
    >
      <Pagination
        total={Math.floor(total / spotifyHelper.PAGE_SIZE) + 1}
        currentPage={Math.floor(offsetNumber / spotifyHelper.PAGE_SIZE) + 1}
        baseHref={`${RoutePath.REPERTOIR}?offset=`}
      />
      <ul className="flex flex-col md:text-center gap-y-1 md:items-center">
        {items.map((list, i) => (
          <Link
            key={list.id}
            className={cx(
              'cursor-pointer hover:text-indigo-700 active:text-indigo-900 focus:text-indigo-800 sm:text-center inline-flex items-center gap-2',
              itemSize === 'small' && 'text-sm',
              itemSize === 'large' && 'text-6xl',
            )}
            href={RoutePath.MIXTAPE.replace(':id', list.id)}
          >
            {i + offsetNumber + 1}
            {list.isPublic ? <GlobeIcon size={'0.5em'} /> : <GlobeLockIcon />}
            {list.name}
          </Link>
        ))}
      </ul>
    </Capsule>
  );
}
