import cx from 'clsx';
interface Props {
  children: React.ReactNode;
}
const SongList = ({ children }: Props) => {
  return (
    <div
      id="song-list"
      className={cx('grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-4')}
    >
      {children}
    </div>
  );
};

export default SongList;
