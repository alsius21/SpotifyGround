import { Capsule } from '@/_components/Capsule';
import cx from 'clsx';

export interface MixtapeLayoutProps {
  title: string;
  description: string;
  children: JSX.Element;
}

const MixtapeLayout = (props: MixtapeLayoutProps): JSX.Element => {
  const { title, description, children } = props;
  const withDescription = description?.length > 0;

  return (
    <Capsule className="mixtape px-0" innerClassName="flex flex-col gap-4">
      <button
        id="mixtape-meta"
        className={cx(
          'cursor-pointer hover:opacity-75 md:flex md:flex-col md:gap-x-1',
        )}
      >
        <span
          className="font-bold mt-auto text-center antialiased"
          id="mixtape-title"
        >
          {cx(title, withDescription && ':')}
        </span>
        {withDescription && (
          <span className={cx('text-center text-sm')} id="mixtape-description">
            {description}
          </span>
        )}
      </button>
      {children}
    </Capsule>
  );
};

export default MixtapeLayout;
