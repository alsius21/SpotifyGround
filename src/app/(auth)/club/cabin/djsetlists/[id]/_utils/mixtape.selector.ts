interface MixtapeItemInfo {
  title: string;
  id: string;
  images: { url: string; width?: number; height?: number }[];
  artists: string[];
  position: number;
  album: string;
  uri: string;
}
type EitherTrackOrEpisode =
  | SpotifyApi.TrackObjectFull
  | SpotifyApi.EpisodeObjectFull;

function isTrackObjectFull(
  obj: EitherTrackOrEpisode,
): obj is SpotifyApi.TrackObjectFull {
  return 'album' in obj && obj.type === 'track';
}

export const selectMixtapeItemInfo = (
  playlist: SpotifyApi.SinglePlaylistResponse,
) => {
  const { tracks, name, description, uri, id } = playlist;
  const items = tracks?.items ?? [];

  return {
    tracks: items
      .map((item) => item.track)
      .filter(isTrackObjectFull)
      .map((track, index): MixtapeItemInfo => {
        const { album, artists, name, id, uri } = track;
        return {
          album: album.name,
          images: album.images.map((img) => ({
            url: img.url,
            width: img.width,
            height: img.height,
          })),
          artists: artists.map((art) => art.name),
          title: name,
          id,
          position: index + tracks.offset,
          uri,
        };
      }),
    name: name ?? '',
    description: description ?? '',
    uri,
    id,
  };
};
