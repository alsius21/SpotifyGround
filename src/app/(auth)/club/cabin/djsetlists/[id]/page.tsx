import { Song } from '@/_components/Song';
import MixtapeLayout from './mixtape.layout';
import SongList from './songList';
import {
  getPlaylist,
  startResumePlayback,
} from '@/_services/spotify.serverclient';
import { selectMixtapeItemInfo } from './_utils/mixtape.selector';

interface Props {
  params: { id: string };
}
const play = async (device_id: string | undefined, uri: string) => {
  'use server';
  await startResumePlayback({
    device_id,
    payload: {
      uris: [uri],
      position_ms: 0,
    },
  });
};
export default async function MixtapePage({ params: { id } }: Readonly<Props>) {
  const playlist = await getPlaylist(id);
  const { tracks, name, description } = selectMixtapeItemInfo(playlist);

  return (
    <MixtapeLayout title={name} description={description}>
      {tracks.length > 0 ? (
        <SongList>
          {tracks.map((track) => {
            return (
              <div className="song-wrapper flex-grow" key={track.id}>
                <Song
                  name={track.title}
                  images={track.images}
                  artists={track.artists}
                  active={false}
                  uri={track.uri}
                  onClick={play}
                />
              </div>
            );
          })}
        </SongList>
      ) : (
        <></>
      )}
    </MixtapeLayout>
  );
}
