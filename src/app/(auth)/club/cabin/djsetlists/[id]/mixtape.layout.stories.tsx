import MixtapeLayout from './mixtape.layout';
import { Meta, StoryObj } from '@storybook/react';

export default {
  title: 'Mixtape/Layout',
  component: MixtapeLayout,
} as Meta<typeof MixtapeLayout>;

export const DefaultMixtapeLayout: StoryObj<typeof MixtapeLayout> = {
  args: {
    title: 'Summer Hits 90',
    description: 'Classics for the greatests and freshest moments',
  },
};
