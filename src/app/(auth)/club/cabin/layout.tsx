import Player from '@/_components/Player';

const CabinLayout = ({ children }: { children: React.ReactNode }) => {
  return (
    <>
      <aside className="md:w-full flex-auto mb-4 sm:mb-0">
        <div className="md:w-[40rem] mx-auto">
          <Player />
        </div>
      </aside>
      <aside>{children}</aside>
    </>
  );
};

export default CabinLayout;
