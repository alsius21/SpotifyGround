'use client';

import { PlayerStateChangedResponse } from '@/_models/index';
import { createContext, useEffect, useState } from 'react';
import { service } from '@/_services/spotify-sdk.service';

export const PlayerContext = createContext<PlayerStateChangedResponse | null>(
  null,
);

const ClientSetup = ({
  accessToken,
  children,
}: {
  accessToken: string;
  children: React.ReactNode;
}) => {
  const [lastState, setLastState] = useState<PlayerStateChangedResponse | null>(
    null,
  );
  useEffect(() => {
    const setupPlaybackSDK = () => {
      return new Promise((resolve) => {
        if (window.Spotify) {
          console.debug('Spotify Player Already Imported');
          resolve(null);
        } else {
          console.debug('Spotify Player is not there');
          service.setup(accessToken, setLastState);
        }
      });
    };
    const handleCleanup = () => {
      service.cleanup();
    };

    setupPlaybackSDK();
    return () => {
      handleCleanup();
    };
  }, []);
  return (
    <PlayerContext.Provider value={lastState}>
      {children}
    </PlayerContext.Provider>
  );
};

export default ClientSetup;
