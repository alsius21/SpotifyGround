import RoutePath from '@/app/routePathEnum';
import Link from 'next/link';

export default function Club() {
  const translations = {
    danceFloor: 'Vibrar a la pista',
    djSetLists: 'Posar-me als plats',
    dressRoom: 'Safareig i màgia',
  };
  return (
    <ul className="flex flex-col h-full gap-8 items-center text-center mt-20">
      <li>
        <Link
          className="flex gap-2 hover:text-indigo-700 active:text-indigo-800"
          href={RoutePath.DANCEFLOOR}
        >
          <i>🕺</i>
          {translations.danceFloor}
        </Link>
      </li>
      <li>
        <Link
          className="flex gap-2 hover:text-indigo-700 active:text-indigo-800"
          href={RoutePath.REPERTOIR}
        >
          <i>🎧</i>
          {translations.djSetLists}
        </Link>
      </li>
      <li>
        <Link
          className="flex gap-2 hover:text-indigo-700 active:text-indigo-800"
          href={RoutePath.DRESSROOM}
        >
          <i>🪞</i>
          {translations.dressRoom}
        </Link>
      </li>
    </ul>
  );
}
