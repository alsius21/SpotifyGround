import CenterLayout from '@/_components/CenterLayout';
import { Toaster } from 'react-hot-toast';

export default function Layout(props: { children: React.ReactNode }) {
  return (
    <>
      <CenterLayout>{props.children}</CenterLayout>
      <Toaster />
    </>
  );
}
