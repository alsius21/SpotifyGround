import React from 'react';
import RedirectionMessage from './redirection-message';
import ClientRedirection from './client-redirection';
/**
 * Component that parses the login data from Spotify
 */
function CheckIdCardPage() {
  return (
    <>
      <RedirectionMessage />
      <ClientRedirection />
    </>
  );
}

export default CheckIdCardPage;
