'use client';
import RoutePath from '@/app/routePathEnum';
import { useRouter } from 'next/navigation';
import { useEffect } from 'react';
const LOADING_CALLBACK_TIME_MS = 1000;
const ClientRedirection = () => {
  const router = useRouter();
  useEffect(function redirection() {
    const id = setTimeout(() => {
      router.push(RoutePath.CLUB);
    }, LOADING_CALLBACK_TIME_MS);
    return () => {
      clearTimeout(id);
    };
  }, []);
  return <></>;
};
export default ClientRedirection;
