import RedirectionMessage from './redirection-message';
import { Meta, StoryObj } from '@storybook/react';
import CenterLayout from '@/_components/CenterLayout';
import { MemoryRouter } from 'react-router-dom';

export default {
  title: 'CheckIdCard/RedirectionMessage',
  component: RedirectionMessage,
} as Meta<typeof RedirectionMessage>;

export const Default: StoryObj<typeof RedirectionMessage> = {
  decorators: [
    (MyStory) => (
      <MemoryRouter initialEntries={['/login']}>
        <MyStory key="story" />
      </MemoryRouter>
    ),
    (MyStory) => (
      <CenterLayout>
        <MyStory />
      </CenterLayout>
    ),
  ],
};
