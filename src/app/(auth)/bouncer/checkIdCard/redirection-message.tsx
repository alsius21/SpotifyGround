import { Capsule } from '@/_components/Capsule';

const messages = {
  redirecting: 'En breus seràs redirigida a les llistes !',
};

interface Props {
  message?: string;
}

const RedirectionMessage = ({
  message = messages.redirecting,
}: Props): JSX.Element => {
  return (
    <Capsule>
      <div className="text-center">{message}</div>
    </Capsule>
  );
};

export default RedirectionMessage;
