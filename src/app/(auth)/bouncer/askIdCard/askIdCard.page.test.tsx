import { render } from '@/test/test-utils';
import AskIdCardPage from './page';

jest.mock('@/_services/environment.provider');

describe('AskIdCard', () => {
  it('renders', () => {
    expect(() =>
      render(<AskIdCardPage params={{ locale: 'en' }} />),
    ).not.toThrow();
  });
});
