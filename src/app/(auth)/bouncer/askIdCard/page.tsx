import { StepInformation } from '@/_components/StepInformation';
import ShowIdCard from './use-case/show-id-card';
import { useTranslation } from '@/_services/i18n';
import { Language, Namespace } from '@/_services/i18n/settings';

interface Props {
  params: {
    locale: string;
  };
}

/**
 * Where user connects with its account
 */
const AskIdCardPage = async (props: Props) => {
  const { t } = await useTranslation(
    props.params.locale as Language,
    Namespace.Connect,
  );
  return (
    <StepInformation
      title={t(`loggedOut.title`)}
      description={t(`loggedOut.description`)}
    >
      <ShowIdCard>{t('loggedOut.next')}</ShowIdCard>
    </StepInformation>
  );
};

export default AskIdCardPage;
