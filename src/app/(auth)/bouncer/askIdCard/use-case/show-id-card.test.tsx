import { render } from '@/test/test-utils';
import ShowIdCard from './show-id-card';
jest.mock('@/_services/environment.provider');

describe('ShowIdCard', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<ShowIdCard>Test Children</ShowIdCard>);
    expect(baseElement).toBeTruthy();
  });
});
