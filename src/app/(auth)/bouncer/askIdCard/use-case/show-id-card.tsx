'use client';
import React from 'react';
import { signIn } from 'next-auth/react';

const ShowIdCard = ({ children }: { children: React.ReactNode }) => {
  return (
    <button
      className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded w-fit mx-auto"
      onClick={(event) => {
        event.preventDefault();
        signIn('spotify', { callbackUrl: '/bouncer/checkIdCard' });
      }}
    >
      {children}
    </button>
  );
};

export default ShowIdCard;
