import React from 'react';
import CenterLayout from '@/_components/CenterLayout';
interface Props {
  children: React.ReactNode;
}
export default function Layout(props: Readonly<Props>) {
  return <CenterLayout>{props.children}</CenterLayout>;
}
