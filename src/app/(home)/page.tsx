import { Title } from '@/_components/index';
import { Capsule } from '@/_components/Capsule';
import { useTranslation } from '@/_services/i18n';
import { Language, Namespace } from '@/_services/i18n/settings';
import RoutePath from '@/app/routePathEnum';

export default async function WelcomePage({
  params: { locale },
}: Readonly<{
  params: { locale: Language };
}>) {
  const { t } = await useTranslation(locale, Namespace.Welcome);

  return (
    <Capsule id="welcome" className="mt-auto mb-auto pb-16">
      <Title
        text={t('title')}
        description={t('description')}
        url={`${RoutePath.ASK_ID_CARD}`}
      />
    </Capsule>
  );
}
