import SpotifyProvider from 'next-auth/providers/spotify';
import environmentProvider from '@/_services/environment.provider';
import { JWT } from 'next-auth/jwt';

/**
 * @see https://www.reddit.com/r/nextjs/comments/10o6aup/next_auth_spotify_reauthentication_access_token/
 */
async function refreshAccessToken(token: JWT): Promise<JWT> {
  if (token.refreshToken === undefined) {
    return {
      ...token,
      error: 'NoRefreshTokenError',
    };
  }
  try {
    const { clientSecret, clientId } = environmentProvider.getClientInfo();
    const basicAuth = Buffer.from(`${clientId}:${clientSecret}`).toString(
      'base64',
    );
    const response = await fetch('https://accounts.spotify.com/api/token', {
      method: 'POST',
      headers: {
        Authorization: `Basic ${basicAuth}`,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: new URLSearchParams({
        grant_type: 'refresh_token',
        refresh_token: token.refreshToken,
      }),
    });

    if (!response.ok) {
      throw new Error(`Failed to refresh token: ${response.statusText}`);
    }

    const data = await response.json();

    return {
      ...token,
      accessToken: data.access_token,
      accessTokenExpires: Date.now() + data.expires_in * 1000,
    };
  } catch (error) {
    return {
      ...token,
      error: 'RefreshAccessTokenError',
    };
  }
}
const baseAuthorizationUrl = 'https://accounts.spotify.com/authorize';
const authorizationUrl = `${baseAuthorizationUrl}?scope=${environmentProvider
  .getScopes()
  .join(',')}`;
export const authOptions = {
  debug: true,
  providers: [
    SpotifyProvider({
      ...environmentProvider.getClientInfo(),
      authorization: authorizationUrl,
    }),
  ],
  callbacks: {
    async jwt({ token, account, user }) {
      if (account && user) {
        return {
          accessToken: account.access_token,
          refreshToken: account.refresh_token,
          accessTokenExpires: account.expires_at * 1000,
          user,
        };
      }
      if (token.accessTokenExpires && Date.now() < token.accessTokenExpires) {
        return token;
      }
      const newToken = await refreshAccessToken(token);
      return newToken;
    },
    async session({ session, token }) {
      session.accessToken = token.accessToken;
      session.error = token.error;
      session.user = token.user;
      return session;
    },
  },
};
