import NextAuth from 'next-auth';
import { authOptions } from './authOptions';

const handler = NextAuth(authOptions);

const wrappedHandler = async function auth(req, res) {
  try {
    return await handler(req, res);
  } catch (error) {
    console.error('NextAuth Error:', JSON.stringify(error));
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

export { wrappedHandler as GET, wrappedHandler as POST };
