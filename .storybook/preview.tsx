import React from 'react';
import { createElement } from 'react';
import { I18nextProvider } from 'react-i18next';
import i18n from './i18n';

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  layout: 'fullscreen',
};

import Layout from './Layout';

const withLayout = (storyFn) => createElement(Layout, null, storyFn());

const withI18next = (Story) => {
  return (
    <I18nextProvider i18n={i18n}>
      <Story />
    </I18nextProvider>
  );
};

export default {
  decorators: [withLayout, withI18next],
};
