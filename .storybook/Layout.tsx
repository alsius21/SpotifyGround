import React from 'react';
import '../src/style.css';
import { Layout as AppLayout } from '../src/_components/Layout';

const Layout = ({ children }) => {
  return <AppLayout>{children}</AppLayout>;
};

export default Layout;
