declare module '*.less?inline' {
  const resource: { [key: string]: string };
  export = resource;
}
declare module '*.scss?inline' {
  const resource: { [key: string]: string };
  export = resource;
}

declare module '*.css' {
  const resource: { [key: string]: string };
  export = resource;
}

declare module '*tailwind.css?inline' {
  const resource: { [key: string]: string };
  export = resource;
}
