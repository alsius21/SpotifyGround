# No tan vintage
Lang: Catalan

Aquest projecte està fet amb el propòsit de poder entregar llistes de Spotify de manera sorpresa. És a dir, sense saber quina serà la següent cançó. 

**Desenvolupament:**

- yarn install
- yarn development
- vercel dev --listen 3001


**Producció:**

- yarn build
- yarn start



**Test de les històries:**

- yarn 'build:tailwind'
- yarn storybook


_Tests inspirats en:_

https://codesandbox.io/s/github/kentcdodds/react-testing-library-examples?file=/src/__tests__/i18next.js:0-35
