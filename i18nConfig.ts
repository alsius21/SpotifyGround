import { Config } from 'next-i18n-router/dist/types';
import { Language } from '@/_services/i18n/settings';

const i18nConfig: Config = {
  defaultLocale: Language.Catalan,
  locales: [Language.Catalan, Language.English, Language.Spanish],
  prefixDefault: true,
};

export default i18nConfig;
